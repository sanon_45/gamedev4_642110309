using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Sanon.GameDev4.UIToolkit
{
    public class Continue : MonoBehaviour
    {
        private UIDocument _uiDocument;

        private VisualElement _startButton;
        
        // Start is called before the first frame update
        void Awake()
        {
            _uiDocument = FindObjectOfType<UIDocument>();
            _startButton = _uiDocument.rootVisualElement.Query<Button>("Continue");
        }

        private void OnEnable()
        {
            _startButton.RegisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
        }

        private void OnStartButtonMouseDownEvent(ClickEvent evt)
        {
            SceneManager.LoadSceneAsync("5.1");
        }

        private void OnDisable()
        {
            _startButton.UnregisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
        }
    }
}